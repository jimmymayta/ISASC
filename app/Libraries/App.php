<?php namespace App\Libraries;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Libraries\Database;
use \DateTimeZone;
use \DateTime;
use \DateInterval;

class App {
    public static function Authentication($User, $Password) {
        $ID_Per = Database::Column('ID_Per');
        $Code_Per = Database::Column('Code_Per');
        $Names_Per = Database::Column('Names_Per');
        $Surnames_Per = Database::Column('Surnames_Per');
        $Email_Per = Database::Column('Email_Per');
        $ID_Sta = Database::Column('ID_Sta');

        $ID_Use = Database::Column('ID_Use');
        $User_Use = Database::Column('User_Use');
        $Password_Use = Database::Column('Password_Use');
        $ID_StaUse = Database::Column('ID_Sta');
        $ID_PerUse = Database::Column('ID_Per');

        $Personal = Database::Table('Personal');
        $Users = Database::Table('Users');

        $Per = DB::select("SELECT Per.$ID_Per AS ID_Per, Per.$Code_Per, Per.$Names_Per, Per.$Surnames_Per, Per.$Email_Per,Per.$ID_Sta,
        Use.$ID_Use, Use.$User_Use, Use.$Password_Use, Use.$ID_StaUse, Use.$ID_PerUse
        FROM $Personal Per, $Users Use
        WHERE Per.$ID_Per = Use.$ID_PerUse AND Use.$User_Use = '$User'
        AND Per.$ID_Sta = 1 AND Use.$ID_StaUse = 1");

        if ($Per && Hash::check($Password, $Per[0]->Password_Use)) {
            return $Per[0];
        } else {
            return null;
        }
    }

    public static function Personal($Code) {
        $Per = DB::table('Personal')
            ->select('ID_Per', 'Names_Per', 'Surnames_Per', 'Email_Per', 'ID_Gro')
            ->where('Code_Per', '=', $Code)
            ->where('ID_Sta', '=', '1')
            ->get();
        return $Per[0];
    }

    public static function Historial($Men) {
        DB::table('historial')->insert([
            'idusuario' => Auth::id(),
            'historial' => $Men,
            'fecha' => App::DateTime()
        ]);
    }

    public static function HistorialDetalle($Data, $historial, $historial_detalle, $descripcion, $acccion) {
        DB::table('historial_detalle')->insert([
            'idusuario' => Auth::id(),
            'id_data' => $Data,
            'historial' => $historial,
            'historial_detalle' => $historial_detalle,
            'descripcion' => $descripcion,
            'acccion' => $acccion,
            'fecha' => App::DateTime()
        ]);
    }

    public static function Session() {
        if (DB::table('Personal')->where('Code_Per', session('Code'))->value('Code_Per')) {
            $Per = DB::table('Personal')
                ->select('ID_Per', 'Names_Per', 'Surnames_Per', 'Email_Per', 'ID_Mod', 'ID_Gro', 'ID_Mon')
                ->where('Code_Per', session('Code'))
                ->where('ID_Sta', 1)
                ->get();
            return $Per[0];
        }
        return null;
    }

    public static function SessionStart($Code) {
        session(['Code' => $Code]);
    }

    public static function SessionEnd() {
        session()->forget('Code');
    }

    public static function Security() {
        if (session('Code')) {
            if ($Code = DB::table('Personal')
                ->where('Code_Per', session('Code'))
                ->value('Code_Per')) {
                $Per = DB::table('Personal')
                    ->select('ID_Per', 'Names_Per', 'Surnames_Per', 'Email_Per',
                        'ID_Mod', 'ID_Gro')
                    ->where('Code_Per', $Code)
                    ->where('ID_Sta', '1')
                    ->get();
                return $Per[0];
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public static function CountDate($Date, $Number) {

    }

    public static function DateTime() {
        $DateTime = new DateTime('now', new DateTimeZone('America/La_Paz'));
        return $DateTime->format('Y-m-d H:i:s');
    }

    public static function DateTime2($Date) {
        $DateTime = new DateTime($Date, new DateTimeZone('America/La_Paz'));
        return $DateTime->format('Y-m-d H:i:s');
    }

    public static function DateDifference($Date) {
        $Date1 = new DateTime($Date, new DateTimeZone('America/La_Paz'));
        $Date2 = new DateTime('now', new DateTimeZone('America/La_Paz'));
        $Diff = $Date1->diff($Date2);
        if ($Date1->format('Y-m-d') > $Date2->format('Y-m-d')) {
            return $Diff->days * -1;
        } else {
            return $Diff->days;
        }
    }

    public static function DateDifference2($Dat1, $Dat2) {
        $Date1 = new DateTime($Dat1, new DateTimeZone('America/La_Paz'));
        $Date2 = new DateTime($Dat2, new DateTimeZone('America/La_Paz'));
        $Diff = $Date1->diff($Date2);
        if ($Date1->format('Y-m-d') > $Date2->format('Y-m-d')) {
            return $Diff->days * -1;
        } else {
            return $Diff->days;
        }
    }

    public static function DateDifference3($Dat1, $Dat2) {
        $Date1 = new DateTime($Dat1, new DateTimeZone('America/La_Paz'));
        $Date2 = new DateTime($Dat2, new DateTimeZone('America/La_Paz'));
        $Diff = $Date1->diff($Date2);
        return $Diff->days;
    }

    public static function AddDay($Date, $Number) {
        $DateTimeZone = new DateTimeZone('America/La_Paz');
        $DateTime = new DateTime($Date, $DateTimeZone);
        $DateInterval = new DateInterval('P'.$Number.'D');
        $DateTime->add($DateInterval);
        return $DateTime->format('Y-m-d');
    }

    public static function Code() {
        $DateTime = new DateTime('now', new DateTimeZone('America/La_Paz'));
        return $DateTime->format('YmdHisu');
    }

    public static function CodeClient() {
        $Data = '0123456789abcdefghjkmnpqrtvwxyzABCDEFGHJKLMNPQRTUVWXYZ';
        $ID = '';
        for ($i = 0; $i < 8; $i++) {
            $ID .= $Data[mt_rand(0, strlen($Data) - 1)];
        }
        return $ID;
    }
}
