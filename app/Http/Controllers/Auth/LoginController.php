<?php namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Libraries\App;

class LoginController extends Controller
{
    public function showLoginForm(){
        return view('auth.login');
    }

    public function login(Request $request){

        $this->validateLogin($request);
        //echo 'User:'.$request->usuario.'<br>Password:'.(Hash::make($request->password));

        if (Auth::attempt(['usuario' => $request->usuario,'password' => $request->password,'condicion'=>1])){
            /* DB::table('historial')->insert([
                'idusuario' => Auth::id(),
                'historial' => 'Inicio sesion',
                'fecha' => App::DateTime()
            ]); */
            App::Historial('Inicio sesion');
            //id_data historial historial_detalle descripcion acccion
            App::HistorialDetalle(Auth::id(), 'Inicio sesion', 'Inicio sesion', 'Inicio sesion', '');
            /* DB::table('historial_detalle')->insert([
                'idusuario' => Auth::id(),
                'historial' => 'Inicio sesion',
                'historial_detalle' => 'Inicio sesion',
                'fecha' => App::DateTime()
            ]); */
            return redirect()->route('main');
        }

         return back()->withErrors(['usuario' => trans('auth.failed')])
         ->withInput(request(['usuario']));
     }

     protected function validateLogin(Request $request){
        $this->validate($request,[
            'usuario' => 'required|string',
            'password' => 'required|string'
        ]);
    }

    public function logout(Request $request){
        App::Historial('Cerro sesion');
        App::HistorialDetalle(Auth::id(), 'Inicio sesion', 'Inicio sesion', 'Inicio sesion', '');
        /*  DB::table('historial')->insert([
            'idusuario' => Auth::id(),
            'historial' => 'Cerro sesion',
            'fecha' => App::DateTime()
        ]); */
        /* DB::table('historial_detalle')->insert([
            'idusuario' => Auth::id(),
            'historial' => 'Cerro sesion',
            'historial_detalle' => 'Cerro sesion',
            'fecha' => App::DateTime()
        ]); */
        Auth::logout();
        $request->session()->invalidate();
        return redirect('/');
    }
}
